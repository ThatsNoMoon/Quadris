local const = {
  window_width = 1280,
  window_height = 720,

  border = 50,
  tile_size = 31,
  board_width = 10,
  board_height = 20,

  quadrimino_speed = 0.3,
}

local state

local function point(x, y)
  return { x = x, y = y }
end

local function new_squares()
  return { 0.3, 0.5, 1.0 }, {
    point(0, 0),
    point(0, 1),
    point(0, 2),
    point(0, 3),
  }
end

local Quadrimino = {}

function Quadrimino:new()
  local color, squares = new_squares()
  local new = {
    x = 5,
    y = 0,
    color = color,
    squares = squares,
  }
  self.__index = self
  return setmetatable(new, self)
end

function Quadrimino:collides(board)
  for _, s in ipairs(self.squares) do
    local x, y = s.x + self.x, s.y + self.y

    if board:get(x, y + 1) or y + 1 == const.board_height then
      return true
    end
  end
  return false
end

function Quadrimino:draw()
  local old_r, old_g, old_b, old_a = love.graphics.getColor()
  love.graphics.setColor(unpack(self.color))
  for _,s in ipairs(self.squares) do
    love.graphics.rectangle(
      "line",
      const.border + ((self.x + s.x) * const.tile_size),
      const.border + ((self.y + s.y) * const.tile_size),
      const.tile_size,
      const.tile_size
    )
  end
  love.graphics.setColor(old_r, old_g, old_b, old_a)
end

function love.load()
  love.window.setMode(const.window_width, const.window_height)

  state = {
    active_quadrimino = Quadrimino:new(),
    last_drop = love.timer.getTime()
  }

  local board = {}

  function board:insert(quadrimino)
    for _,s in ipairs(quadrimino.squares) do
      local x, y = quadrimino.x + s.x, quadrimino.y + s.y
      print(string.format(""))
      self[const.board_width * (quadrimino.y + s.y) + (quadrimino.x + s.x)] = quadrimino.color
    end
  end

  function board:get(x, y)
    return self[const.board_width * x + y]
  end

  state.board = board
end

function love.update()
  if love.keyboard.isDown("escape") then
    love.event.quit()
    return
  end

  if love.timer.getTime() - state.last_drop >= const.quadrimino_speed then
    if state.active_quadrimino then
      if state.active_quadrimino:collides(state.board) then
        state.board:insert(state.active_quadrimino)
        print(string.format("quadrimino fell at (%i, %i)", state.active_quadrimino.x, state.active_quadrimino.y))
        state.active_quadrimino = nil
      else
        state.active_quadrimino.y = state.active_quadrimino.y + 1
        state.last_drop = love.timer.getTime()
      end
    else
      state.active_quadrimino = Quadrimino:new()
    end
  end
end

function love.draw()
	love.graphics.rectangle(
    "line",
    const.border,
    const.border,
    const.tile_size * const.board_width,
    const.tile_size * const.board_height
  )
  if state.active_quadrimino then
    state.active_quadrimino:draw()
  end
  for i = 1, const.board_width * const.board_height do
    if state.board[i] then
      local old_r, old_g, old_b, old_a = love.graphics.getColor()
      love.graphics.setColor(unpack(state.board[i]))
      local x, y = i % const.board_width, math.floor(i / const.board_height)
      print(string.format("drawing square at (%i, %i)", x, y))
      love.graphics.rectangle(
        "line",
        const.border + const.tile_size * x,
        const.border + const.tile_size * y,
        const.tile_size,
        const.tile_size
      )
      love.graphics.setColor(old_r, old_g, old_b, old_a)
    end
  end
end
